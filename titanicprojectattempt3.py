# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 15:15:19 2019

@author: Tim
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Sep 23 14:25:00 2019

@author: Tim
"""
import warnings
warnings.filterwarnings("ignore")


import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from sklearn.metrics import confusion_matrix
import pandas as pd
import numpy as np
import math 


np.random.seed(2019)
from scipy.stats import skew
from scipy import stats

import statsmodels
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report

import matplotlib.pyplot as plt
import re

#models
from sklearn.model_selection import StratifiedShuffleSplit,train_test_split
from sklearn.metrics import accuracy_score, log_loss
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier,AdaBoostClassifier,GradientBoostingClassifier
from sklearn.model_selection import KFold, cross_val_score
from sklearn.model_selection import StratifiedKFold

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from xgboost import XGBClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis

print("done")


#loading data
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')
PassengerId = test['PassengerId']
train.head(3)

#combining data 
full_data = [train, test]
datacombined = pd.concat(full_data)


#Looking at number of people who survived:
datacombined['Survived'].value_counts()


#Looking at number of people of different class:
datacombined['Pclass'].value_counts()


#Hypothesis: rich people survived at a higher rate
figure, myaxis = plt.subplots()
sns.countplot(x = "Pclass", hue="Survived",data = train,  ax = myaxis)
myaxis.set_title("Passenger Class Distribution - Survived vs Not-survived")
myaxis.set_xlabel("Pclass")
myaxis.set_ylabel("Total Count")
myaxis.legend(["Not Survived", "Survived"], loc = 'upper right')



#Looking at names column:
datacombined['Name'].nunique()

#Two duplicates here
duplicatenames=datacombined[datacombined.duplicated(subset='Name', keep=False)]
print(duplicatenames)
#These look like coincindences, keep going


#Looking into Miss which appears in multiple names:
misses= datacombined[datacombined["Name"].str.contains('Miss.')]

mrses = datacombined[datacombined["Name"].str.contains('Mrs.')]


#Now looking at males to see if there's a similar idea there:
males= datacombined.loc[datacombined['Sex'] == 'male']



#feature engineering titles to numbers from 0 to 4
datacombined['Title'] = datacombined.Name.str.extract(' ([A-Za-z]+)\.', expand=False)
datacombined['Title'] = datacombined['Title'].replace(['Capt', 'Col', 'Countess', 'Don',
                                               'Dr', 'Dona', 'Jonkheer', 
                                                'Major','Rev','Sir'],'Rare') 
datacombined['Title'] = datacombined['Title'].replace(['Mlle', 'Ms','Mme'],'Miss')
datacombined['Title'] = datacombined['Title'].replace(['Lady'],'Mrs')
datacombined['Title'] = datacombined['Title'].map({"Mr":2, "Rare" : 4, "Master" : 0,"Miss" : 1, "Mrs" : 3 })
    
#Look at info on age
print(datacombined[1:891].describe())



#Plotting survival rates with age and faceting with sex as well as class
g = sns.FacetGrid(train, col="Pclass", row="Sex", hue="Survived")
(g.map(plt.hist, "Age")).add_legend();


#Want to validate that Master is a good proxy for boys
boys = datacombined.loc[datacombined['Title'] == 2]
boys.describe()
#True

#Now examining Misses:
misses = datacombined.loc[datacombined['Title'] == 3]
misses.describe()
#Misses can be young girls but also some young female adults, probably not yet married

#Looking at number of misses with different ages
g = sns.FacetGrid(misses, col="Pclass", hue="Survived")
(g.map(plt.hist, "Age")).add_legend();

#seeing if alone misses have higher chances of survival
missesalone = misses.loc[(misses['Parch']==0) & (misses['SibSp']==0)]
missesalone["Age"].describe()



#investigating sibsp variable:
datacombined['SibSp'].describe()

g = sns.FacetGrid(datacombined[1:891], col="Title", row="Pclass", hue="Survived")
(g.map(plt.hist, "SibSp")).add_legend();


#Now visualise the Parch variable in the same way:
g = sns.FacetGrid(datacombined[1:891], col="Title", row="Pclass", hue="Survived")
(g.map(plt.hist, "Parch")).add_legend();

#Feature engineering: going to create a family size feature:
datacombined['Familysize'] = datacombined['SibSp']+datacombined['Parch']+1

#Looking at how survival rates depend on the family size:
g = sns.FacetGrid(datacombined[1:891], col="Pclass",row = 'Title', hue="Survived")
(g.map(plt.hist, "Familysize")).add_legend();



#There appears to be no useful information from the ticket number. Going to try to first take
#the first letter and see if it means anything
ticketfirstchar = datacombined['Ticket'].str[:1].astype('category')
datacombined['TicketFirstChar'] = ticketfirstchar
datacombined.dtypes

#First some plots of this to get any ideas:
g = sns.FacetGrid(datacombined[1:891], hue="Survived")
(g.map(sns.countplot, "TicketFirstChar")).add_legend()

#Now separate by class:
g = sns.FacetGrid(datacombined[1:891], col='Pclass', hue="Survived")
(g.map(sns.countplot, "TicketFirstChar")).add_legend()

#Finally, add in title to the mix
g = sns.FacetGrid(datacombined[1:891], col='Pclass', row= 'Title', hue="Survived")
(g.map(sns.countplot, "TicketFirstChar")).add_legend()


#Next looking into fares paid by passengers:
datacombined['Fare'].describe()
g = sns.FacetGrid(datacombined[1:891], hue="Survived")
(g.map(plt.hist, "Fare")).add_legend()

#separate by class and title:
g = sns.FacetGrid(datacombined[1:891],col='Pclass', row= 'Title', hue="Survived")
(g.map(plt.hist, "Fare")).add_legend()


#Analysis of cabin variable:
#Taking first character in same way as for ticket:
cabinfirstchar = datacombined['Cabin'].str[:1].astype('category')
cabinfirstchar = cabinfirstchar.cat.add_categories('Unknown')
cabinfirstchar.fillna("Unknown", inplace=True)
datacombined['CabinFirstChar'] = cabinfirstchar


g = sns.FacetGrid(datacombined, hue="Survived")
(g.map(sns.countplot, "CabinFirstChar")).add_legend()

#Now separate also by class
g = sns.FacetGrid(datacombined,col='Pclass', hue="Survived")
(g.map(sns.countplot, "CabinFirstChar")).add_legend()

#add title into the mix
g = sns.FacetGrid(datacombined,col='Pclass',row='Title', hue="Survived")
(g.map(sns.countplot, "CabinFirstChar")).add_legend()

#It appears that there are people with multiple cabins. Going to look at those in particular
#intuition is that these are more rich people and surivived much more:
multiplecabins = datacombined['Cabin'].str.contains(' ').astype('category')
datacombined['MultipleCabins'] = multiplecabins
datacombined['MultipleCabins'].describe()

g = sns.FacetGrid(datacombined,col='Pclass',row='Title', hue="Survived")
(g.map(sns.countplot, "MultipleCabins")).add_legend()


#Does survivability depend on where a passenger embarked?
datacombined['Embarked'] = datacombined['Embarked'].astype('category')
datacombined.dtypes

g = sns.FacetGrid(datacombined,col='Pclass',row='Title', hue="Survived")
(g.map(sns.countplot, "Embarked")).add_legend()



#Beginning exploratory modelling. Using randomforest.
#Going to create a dataframe with just class and title for the first go
datacombined['Title'] = datacombined['Title'].astype('category')
datacombined['Pclass'] = datacombined['Pclass'].astype('category')
datacombined['Survived'] = datacombined['Survived'].astype('category')
rftrain1 = datacombined[:891].loc[:,['Title', 'Pclass']]
rflabel = datacombined[:891].loc[:,['Survived']]


#Perform first random forest
rf1 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf1.fit(rftrain1,rflabel)
ypred1 = rf1.predict(rftrain1)

print(confusion_matrix(rflabel,ypred1))
print(classification_report(rflabel,ypred1))
print(accuracy_score(rflabel, ypred1))
print(rf1.feature_importances_)


#Now making a second model which also takes into account the sibsp variable which it appeared may have predictive ability
rftrain2 = datacombined[:891].loc[:,['Title', 'Pclass', 'SibSp']]
rf2 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf2.fit(rftrain2,rflabel)
ypred2 = rf2.predict(rftrain2)

print(confusion_matrix(rflabel,ypred2))
print(classification_report(rflabel,ypred2))
print(accuracy_score(rflabel, ypred2))
print(rf2.feature_importances_)


#Giving the parch variable a go instead of sibsp
rftrain3 = datacombined[:891].loc[:,['Title', 'Pclass', 'Parch']]
rf3 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf3.fit(rftrain3,rflabel)
ypred3 = rf3.predict(rftrain3)

print(confusion_matrix(rflabel,ypred3))
print(classification_report(rflabel,ypred3))
print(accuracy_score(rflabel, ypred3))
print(rf3.feature_importances_)

#Both have predictive power, but Sibsp is more powerful than Parch
#Now we try use both
rftrain4 = datacombined[:891].loc[:,['Title', 'Pclass', 'SibSp','Parch']]
rf4 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf4.fit(rftrain4,rflabel)
ypred4 = rf4.predict(rftrain4)

print(confusion_matrix(rflabel,ypred4))
print(classification_report(rflabel,ypred4))
print(accuracy_score(rflabel, ypred4))
print(rf4.feature_importances_)

print(rf4.oob_score_)
#Using both is even better, however now going to replace both with the created family size feature


rftrain5 = datacombined[:891].loc[:,['Title', 'Pclass', 'Familysize']]
rf5 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf5.fit(rftrain5,rflabel)
ypred5 = rf5.predict(rftrain5)

print(confusion_matrix(rflabel,ypred5))
print(classification_report(rflabel,ypred5))
print(accuracy_score(rflabel, ypred5))
print(rf5.feature_importances_)


#new model including familysize as well as sibsp
rftrain6 = datacombined[:891].loc[:,['Title', 'Pclass', 'Familysize', 'SibSp']]
rf6 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf6.fit(rftrain6,rflabel)
ypred6 = rf6.predict(rftrain6)

print(confusion_matrix(rflabel,ypred6))
print(classification_report(rflabel,ypred6))
print(accuracy_score(rflabel, ypred6))
print(rf6.feature_importances_)



#swittch sibsp out for parch
rftrain7 = datacombined[:891].loc[:,['Title', 'Pclass', 'Familysize', 'Parch']]
rf7 = RandomForestClassifier(n_estimators = 1000, random_state = 1234)
rf7.fit(rftrain7,rflabel)
ypred7 = rf7.predict(rftrain7)

print(confusion_matrix(rflabel,ypred7))
print(classification_report(rflabel,ypred7))
print(accuracy_score(rflabel, ypred7))
print(rf7.feature_importances_)



#Making a crossvalidation function to help automate cv. Can also use OOB for an estimate for the error
#but here sticking to this method. This may be something to try next time:
def cross_val_metrics(fit, training_set, class_set, estimator, print_results = True):
    my_estimators = {
    'rf': 'estimators_',
    'nn': 'out_activation_',
    'knn': '_fit_method'
    }
    try:
        # Captures whether first parameter is a model
        if not hasattr(fit, 'fit'):
            return print("""'{0}' is not an instantiated model from scikit-learn""".format(fit)) 

        # Captures whether the model has been trained
        if not vars(fit)[my_estimators[estimator]]:
            return print("""Model does not appear to be trained.""")

    except KeyError as e:
        print("""'{0}' does not correspond with the appropriate key inside the estimators dictionary. \
\nPlease refer to function to check `my_estimators` dictionary.""".format(estimator))
        raise

    n = KFold(n_splits=10)
    scores = cross_val_score(fit, 
                         training_set, 
                         class_set, 
                         cv = n)
    if print_results:
        for i in range(0, len(scores)):
            print("""Cross validation run {0}: {1: 0.3f}""".format(i, scores[i]))
        print("""Accuracy: {0: 0.3f} (+/- {1: 0.3f})"""\
              .format(scores.mean(), scores.std() / 2))
    else:
        return scores.mean(), scores.std() / 2

cross_val_metrics(rf4, 
                  rftrain4, 
                  rflabel, 
                  'rf',
                  print_results = True)